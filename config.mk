PRODUCT_SOONG_NAMESPACES += \
    vendor/ANXCamera

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/ANXCamera/proprietary/etc,$(TARGET_COPY_OUT_SYSTEM)/etc) \
    $(call find-copy-subdir-files,*,vendor/ANXCamera/proprietary/lib64,$(TARGET_COPY_OUT_SYSTEM)/lib64) \
    $(call find-copy-subdir-files,*,vendor/ANXCamera/proprietary/priv-app/ANXCamera/lib,$(TARGET_COPY_OUT_SYSTEM)/priv-app/ANXCamera/lib) \
    $(call find-copy-subdir-files,*,vendor/ANXCamera/proprietary/vendor/lib64,$(TARGET_COPY_OUT_VENDOR)/lib64) \
    $(call find-copy-subdir-files,*,vendor/ANXCamera/proprietary/lib,$(TARGET_COPY_OUT_SYSTEM)/lib) \
    $(call find-copy-subdir-files,*,vendor/ANXCamera/proprietary/system_ext/lib,$(TARGET_COPY_OUT_SYSTEM_EXT)/lib)

PRODUCT_PACKAGES += \
    ANXCamera

include vendor/ANXCamera/BoardConfigVendor.mk
